.. _base_mods:

Base modules
=============

Query
-------
  .. automodule:: entrezpy.base.query
    :members:
    :undoc-members:


Parameter
-------------
  .. automodule:: entrezpy.base.parameter
    :members:
    :undoc-members:



Request
------------
  .. automodule:: entrezpy.base.request
    :members:
    :undoc-members:

Analyzer
------------
  .. automodule:: entrezpy.base.analyzer
    :members:
    :undoc-members:


Result
-----------
  .. automodule:: entrezpy.base.result
    :members:
    :undoc-members:

Monitor
-------
  ..  automodule:: entrezpy.base.monitor
      :members:
      :undoc-members:
